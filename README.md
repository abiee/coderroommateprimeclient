Coder Room Mate Web Client Mocks
===================
This repo contains only the mocks to be implemented in CoderRoomMate web client in the near future.

See the mocks
-------------
To see the mocks you need to run this project with grunt, if you don´t have installed grunt this is a good moment to do it, do not forget to install bower too.

    $ npm install -g grunt-cli
    $ npm install -g bower

Once grunt is installed, please install the project dependencies.

    $ npm install
    $ bower install

Now that all dependencies all installed. Run it!

    $ grunt serve

Note: This project was bootstrapped with [Yeoman](http://yeoman.io/), please see Yeoman documentation for more information.
